/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prograjson;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author catalinaaguayo
 */
public class PrograJson {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int Tel[] = {11,22};
        Ciudad c1=new Ciudad("Temuco");
        Persona p1 = new Persona("Catalina","Aguayo","11.111.111-1",Tel,new Ciudad ("Temuco"));
        Persona p2 = new Persona("Juan","Perez","12.112.112-2",Tel,new Ciudad ("Pucon"));
        Persona p3 = new Persona("Pedro","Munoz","13.113.113-3",Tel,new Ciudad ("Villarrica"));
        
        Persona APersona[] = {p1,p2,p3};

        //Objeto a Json
        Gson gson = new Gson();
        String json = new Gson().toJson(APersona);
        System.out.println(json);     
        System.out.println("");
        
        //Json a Objeto
        Persona valores[]=gson.fromJson(json,Persona[].class);
        for (int i = 0; i < valores.length; i++) {
            System.out.println(valores[i].toString());
            
        }
        
    }
    
    
    
}
